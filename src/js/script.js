$('#mobile-menu').click(function(){
    $('ul#navigation').toggleClass("active");
});

$('#mobile-menu-close').click(function(){
    $('ul#navigation').removeClass("active");
});

var slide = 1;
datSlideShowDoe(slide);
function datSlideShowDoe(slideIndex) {
	var i;
	var slide;
	var slides = document.getElementsByClassName("slide");
	var circles = document.getElementsByClassName("twitter-circle");

	if (slideIndex > slides.length) {
	    slide = 1;
	}

	if (slideIndex < 1) {
	    slide = slides.length;
	}

	for (i = 0; i < slides.length; i++) {
	    slides[i].style.display = "none";
	}

	for (i = 0; i < circles.length; i++) {
	    circles[i].className = circles[i].className.replace(" active", "");
	}

	slides[slideIndex-1].style.display = "block";  
	circles[slideIndex-1].className += " active";
}